package com.example.administrator.vpn3

import android.app.Application
import android.util.Log
import okhttp3.*
import org.w3c.dom.Node
import java.io.ByteArrayInputStream
import java.io.IOException
import javax.xml.parsers.DocumentBuilderFactory

/**
 * Created by Alexander Mozhugov on 25.05.2018.
 */
class App : Application() {
    val TAG: String = this.javaClass.simpleName
    var servers: MutableList<Server>? = null
    var callback: ServersCallback? = null
    override fun onCreate() {
        super.onCreate()
        instance = this
        requestServers()
    }

    public fun getServers(callback: ServersCallback) {
        if (servers == null) {
            requestServers()
            this.callback = callback
        } else {
            this.callback = null
            callback.callback(servers!!)
        }
    }

    private fun requestServers() {
        val httpClient = OkHttpClient()
        val url = "https://www.wlvpn.com/serverList.xml"
        val request = Request.Builder()
                .url(url)
                .build()

        httpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e(TAG, "error in getting response using async okhttp call")
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                val xml = response.body()!!.string()
                val d = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                        .parse(ByteArrayInputStream(xml.toByteArray()))
                val s = d.getElementsByTagName("server")
                servers = mutableListOf()
                for (i: Int in 0 until s.length) {
                    val server = convertToServer(s.item(i))
                    servers!!.add(server)
                }
                if (callback != null) {
                    callback!!.callback(servers!!)
                }
            }
        })
    }

    private fun convertToServer(n: Node): Server {
        val attrs = n.attributes
        val name: String = attrs.getNamedItem("name").nodeValue
        val capacity: Int = attrs.getNamedItem("capacity").nodeValue.toInt()
        val city: String = attrs.getNamedItem("city").nodeValue
        val country: String = attrs.getNamedItem("country").nodeValue
        val icon: String = attrs.getNamedItem("icon").nodeValue
        val ip: String = attrs.getNamedItem("ip").nodeValue
        val status: Int = attrs.getNamedItem("status").nodeValue.toInt()
        val visible: Int = attrs.getNamedItem("visible").nodeValue.toInt()
        val lat: Double = attrs.getNamedItem("lat").nodeValue.toDouble()
        val lng: Double = attrs.getNamedItem("lng").nodeValue.toDouble()
        val s = Server(name, capacity, city, country, icon, ip, status, visible, lat, lng)
        return s
    }

    companion object {
        public lateinit var instance: App
    }
}

interface ServersCallback {
    fun callback(servers: List<Server>)
}

data class Server(val name: String, val capacity: Int, val city: String, val country: String,
                  val icon: String, val ip: String, val status: Int, val visible: Int,
                  val lat: Double, val lng: Double)