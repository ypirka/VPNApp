package com.example.administrator.vpn3

import android.app.Activity
import android.app.Notification
import android.os.Bundle
import android.util.Log
import com.gentlebreeze.vpn.sdk.VpnSdk
import com.gentlebreeze.vpn.sdk.config.SdkConfig
import com.gentlebreeze.vpn.sdk.model.*


class MainActivity : Activity() {
    val TAG: String = this.javaClass.simpleName;
    val ACCOUNT_NAME = "privacysrl"
    val API_KEY: String = "01e65a0dbdfcd90ac81d8d5068060d59"
    val userName: String = ""
    var password: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sdk = VpnSdk.init(application, SdkConfig(ACCOUNT_NAME,
                API_KEY))

        /*когда я создавала сервер, не уверена что верно указала параметры.
        во-первых, праивльно ли я вставила popName?
        во-вторых, isInMaintenance - я посчитала что true (типо поддерживается)
        в-третьих, sheduledMaintenance - не знаю какое значение установить*/
        val myServer = VpnServer("myServer", "nrt-a01.wlvpn.com", "173.245.217.6",
                true, 300, 25)
        // создала порт, не уверена что вообще нужно было делать это, но чтобы использовать
        // метод connect (они ниже), там нужно и порт указать в конструкторе параметра
        // VpnConnectionConfiguration. Указала 8080
        val port = VpnPortOptions(8080)

        // чисто список из одного сервера
        val vpnServers: List<VpnServer> = listOf(myServer)

        sdk.fetchAllServers().subscribe(
                { vpnServers
                    Log.d(TAG, vpnServers.toString())
                }, { e: Throwable ->
            Log.e(TAG, "error fetchAllServers")
        })

        //для начала я не знаю какой userName и password указывать, поэтому в начале указала пустую строку
        // к тому же я не понимаю к чему scrambleOn (не укладываетя перевод этого слова и то как его использвоать)
        // и выбрала протокол TCP, но возможно нужно было UDP?
        sdk.connect(myServer, VpnNotification(Notification(), Notification.BADGE_ICON_LARGE),
                VpnConnectionConfiguration(userName, password,false, false,
                        port, VpnProtocolOptions.PROTOCOL_TCP))

        //Log.d(TAG, sdk.getConnectionInfo().toString())
        Log.d(TAG, sdk.isConnected().toString())
        Log.d(TAG, "check")

        val callback = sdk.listenToConnectState()
                .onNext {
                    // do something
                }.onError {
                    // handle error
                }.subscribe()

        callback.unsubscribe()

    }

    override fun onResume() {
        super.onResume()
        App.instance.getServers(object : ServersCallback {
            override fun callback(servers: List<Server>) {
                Log.d(TAG, "Server list got : " + servers.size)
                servers.forEach { Log.d(TAG, it.toString()) }
            }
        })
    }

}


